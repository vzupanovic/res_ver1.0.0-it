
#include "rescaffold.h"
#include <time.h>
#include <string.h>
#include <stdio.h>

#define NO_THRSH -1000

/*
 * Main module which collects user input and passes it to
 * rescaffolder or preprocessor depending on the threshold
 * value - t.
 * */

using namespace std;

static void usage(void){
	fprintf(stderr,
	("Program usage: ./nesto2 {PATH_TO_FILES | OPTIONS}\n"
	  "	PATH_TO_FILES := contig_file scaffold_file cluster_file lib_file\n"
	  "	OPTIONS := -t [set treshold | put NO_THRSH for no threshold] \n" 
	  "	-t treshold_size (-41,inf)\n\n"
	));

}


int main(int argc, char * argv[]){
	clock_t start, finish;
	start = clock();
	int command;
	int thrsh;
	bool thrshProvided;
	if (argc < 6){
                cerr << "Wrong invocation of the program." << endl;
                usage();
                exit(1);
        }
	cout << "Process started." << endl;
	while ((command = getopt(argc, argv, "t:")) >= 0) {
		switch(command){
			case 't': 
				if (strcmp(optarg, "NO_THRSH") == 0)
					thrsh = NO_THRSH;
				else if (atoi(optarg) < -41){
						cout << "Minimal value for threshold is -41.\nExiting due to the wrong thrsh. value\n" << endl;
						exit(-1);
					}
				else
					thrsh = atoi(optarg);
				break;
			default:
				cout << "Unknown command exiting." << endl; exit(-1); 
		}
	}
	const char * contigFileName = argv[optind];
	const char * scaffoldFileName = argv[optind + 1];
	const char * clusterFileName = argv[optind + 2];
	const char * libFileName = argv[optind + 3];
	if (thrsh != NO_THRSH)
		rescaffold("src/preprocessor/res_scaffolds.scaf", contigFileName, scaffoldFileName, clusterFileName, libFileName, thrsh);
	else
		rescaffold("src/res_scaffolds.scaf", contigFileName, scaffoldFileName, clusterFileName, libFileName, thrsh);
	cout << "Total execution time: " <<  ( (double)clock() - start ) / CLOCKS_PER_SEC << " sec"  << endl;
	return 0;
}
