#pragma once

#include <list>
#include <map>
#include <time.h>
#include <algorithm>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <vector>

#include "QuadProg.h"
#include "Contig.h"
#include "PET.h"


using namespace std;
using namespace QuadProgPP;

class GapEstimater
{
public:
	GapEstimater(void);
	~GapEstimater(void);

	// Attributes
private:
	vector<Contig*> *m_contigs;
	map<string, Contig*> *m_contigsName;
	list<PET*> *m_edges;
	string m_scafName;
	double m_resultValue;

	// Methods
 public:
	int ReadScaffolds( string scafFile );
	int ReadClusters( string clusterFile );
	void CalculateGap( int kmer );                               // calculate the gap sizes
	int OutputScaffolds( string outputFile );
};
