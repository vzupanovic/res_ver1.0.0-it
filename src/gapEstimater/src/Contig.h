#pragma once

#include <string>
#include <list>

class PET;
#include "PET.h"
#include "CommonFunction.h"

using namespace std;

class Contig
{
public:
	Contig(void);
	Contig( string name, double length, double cov );
	~Contig(void);

	// Attributes
private:
	string m_name;		// contig name
	double m_length;		// contig length
       	int m_id;			// contig ID
	list<PET*> *m_edges;		// the edges
	int m_ori;			// orientation in scaffold
	
	//***********gap sizes related
	int m_scaffoldPos;			// the order in scaffolds
	int m_gapSize;				// the gap size after this scaffold
	//***********

	// Methods
public:
	void SetName( string name );
	string GetName();
	void SetLength( double length );
	double GetLength();
	void SetID( int id );
	int GetID();
	void AddEdge( PET *p );			// add a pet cluster of this contig
	list<PET*> * GetEdges();	
	void SetOri( int ori );
	int GetOri();
	
	// gap related
	void SetScaffoldPos( int pos );
	int GetScaffoldPos();
	void SetGap( int gap );
	int GetGap();
       
	string toScaffoldString();
};
