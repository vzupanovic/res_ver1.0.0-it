#include "PET.h"

PET::PET(void)
{
}

PET::~PET(void)
{
}

PET::PET( Contig *c1, int o1, Contig *c2, int o2, int dis, int std, int size ){
	m_startContig = c1;
	m_startContigOri = o1;
	m_endContig = c2;
	m_endContigOri = o2;
	m_distance = dis;
	m_std = std;
	m_size = size;
}

int PET::GetSize(){
	return m_size;
}

Contig * PET::GetStartContig(){
	return m_startContig;
}

Contig * PET::GetEndContig(){
	return m_endContig;
}

int PET::GetStartContigOri(){
	return m_startContigOri;
}

int PET::GetEndContigOri(){
	return m_endContigOri;
}

// get the positon of contig c: start or end
int PET::GetPositionOfContig( Contig *c ){
	if( m_startContig == c )
		return START;
	else
		return END;
}

// get the orientation of contig c
int PET::GetOrientationOfContig( Contig *c ){
	if( m_startContig == c )
		return m_startContigOri;
	else
		return m_endContigOri;
}

void PET::SetDis( int dis ){
	m_distance = dis; 
}

int PET::GetDis(){
	return m_distance;
}

// get the other contig rather than c
Contig* PET::GetOtherContig( Contig *c ){
	if( m_startContig == c )
		return m_endContig;
	else
		return m_startContig;
}


// get the standard deviation of this edge
int PET::GetStd(){
	return m_std;
}

void PET::SetOriString( string s ){
	m_oriString = s;
}

string PET::GetOriString(){
	return m_oriString;
}
