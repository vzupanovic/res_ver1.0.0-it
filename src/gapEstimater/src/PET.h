#pragma once

// the paired-end reads cluster, used as edge in graph
#include <string>
class Contig;
#include "Contig.h"
#include "CommonFunction.h"

using namespace std;

class PET
{
public:
	PET(void);
	PET( Contig *c1, int o1, Contig *c2, int o2, int dis, int std, int size );
	~PET(void);

	// Method
public:
	Contig * GetStartContig();
	Contig * GetEndContig();
	int GetSize();
	int GetStartContigOri();
	int GetEndContigOri();
	int GetPositionOfContig( Contig *c );		// get the positon of contig c: start or end
	int GetOrientationOfContig( Contig *c );
	void SetDis( int dis );
	int GetDis();
	Contig* GetOtherContig( Contig *c );			// get the other contig rather than c
	int GetStd();
	void SetOriString( string s );
	string GetOriString();


	// Attributes
private:
	Contig *m_startContig;
	int m_startContigOri;
	Contig *m_endContig;
	int m_endContigOri;
	int m_distance;
	int m_std;
	int m_size;
	string m_oriString;			// original cluster string	
};
