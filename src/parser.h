#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

/*
 * Parser: very simple class used to get the 
 * the mean and the st. deviation of the library 
 * that are maximising the expression inside
 * getSum method.
 * */

class Parser{
	private:
	int getSum(int mean, int stDev){
		return mean + 6 * stDev;
	}
	
	public:
	int mean;
	int stDev;
	int sum;
	string line;
	vector<string> tokens;
	
	Parser(const char *fileName, int& mean, int& stDev, int& globalSum){
		mean = 0;
		stDev = 0;
		globalSum = 0;
		ifstream inFile(fileName);
		if (!inFile){
			cerr << "Can't open lib file, exiting..." << endl;
			exit(1);
		}
		
		int currentMean = 0;
		int currentDev = 0;
		
		while(getline(inFile, line)){
			int meanExt = line.find("Mean");
			int standardExt = line.find("Standard");
			if (meanExt > -1){
				boost::algorithm::split(tokens, line, boost::algorithm::is_any_of(" "));
				currentMean = boost::lexical_cast<int>(tokens[6]);
			}
			if (standardExt > -1){
				boost::algorithm::split(tokens, line, boost::algorithm::is_any_of(" "));
				currentDev = boost::lexical_cast<int>(tokens[6]);
			}
			if (line.empty()){
				int sum = getSum(currentMean, currentDev);
				if (sum > globalSum){
					mean = currentMean;
					stDev = currentDev;
					globalSum = sum;
				}
			}
		}
	}
};

#endif /*PARSER_H*/

