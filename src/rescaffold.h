#ifndef RESCAFFOLD_H
#define RESCAFFOLD_H

#include "local_search.h"
	
	void getStatistics(LocalSearch * localSearch);
	int ifHiasKey(int id);
	void outputScaffoldFile(const char * fileName, LocalSearch * localSearch);
	void mappContigsWithScaffoldData(const char * fileName, const char * fastaName);
	void createSuperContigs(const char * fileName, const char * contigFileName, const char * scaffoldFileName, const char * clusterFileName, int thrsh);
	void rescaffold(const char * fileName, const char * contigFileName, const char * scaffoldFileName, const char * clusterFileName, const char * libFileName, int thrsh);
	void generateFastaFile(const char * fileName, const char * fastaName);


#endif /*RESCAFFOLD_H*/
