#ifndef SCAFFOLD1_H
#define SCAFFOLD1_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>

#include "sub_scaffold.h"

/*
 * Scaffold - class that mimics the real scaffold.
 * It contains vector of pointers to partial scaffolds
 * and vector of pointer to contigs that are part of
 * current scaffold. 
 * */

class Scaffold{
	public:
	vector<Contig *> scaffold;
	vector<SubScaffold *> partialScaffold;
	int id;
	
	Scaffold(int ID){
		id = ID;
	}
	
	void addContigToScaffold(Contig *contig){
		scaffold.push_back(contig);
	}
	
	void addPartialScaffold(SubScaffold * subscaffold){
		partialScaffold.push_back(subscaffold);
	}
	
	~Scaffold(){
		for (vector<Contig *>::const_iterator it = scaffold.begin(); it != scaffold.end(); it++){
			delete *it;
		}
		for (int i = 0; i < partialScaffold.size(); i++){
			delete partialScaffold[i];
		}
		
	}
	
	vector<Contig *> * getScaffold(){
		return &(scaffold);
	}
	
	vector<SubScaffold *> * getAllSubScaffold(){
		return &(partialScaffold);
	}
	
	
};

#endif /*SCAFFOLD1_H*/
