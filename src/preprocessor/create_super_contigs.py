from loader import *
from scaffold_graph import *
import math
import sys
import os.path

class Preprocessor:
	def __init__(self, scaffoldFile, contigFile, clusterFile):
		loader = Loader(scaffoldFile, contigFile, clusterFile)
		self.edgeData, self.edgeLinkers = loader.loadClusterFile()
		self.contigData = loader.loadContigFile()
		self.scaffoldData, self.orderedContigsList, self.origin, self.contigInfo, self.extractedScaffolds = loader.loadScaffoldFile()
		self.scaffoldGraphObject = ScaffoldGraph(self.contigData, self.scaffoldData, self.edgeData)
		self.mergeContigs()

	def filterUnecessaryEdges(self):
		validEdges = []
		for edge in self.edgeData:
			if (int(edge[-1] > size)):
				validEdges.append(edge)
		self.edgeData = validEdges[:]

	def mergeContigs(self):
		print self.scaffoldData

if __name__ == "__main__":
	clusterFile = "/mnt/ScratchPool/gisv61/ver2_gaps_play/datasets/drosophila/all_edge.dat"
	scaffoldFile = "/mnt/ScratchPool/gisv61/ver2_gaps_play/datasets/drosophila/drosophila.scaf"
	contigFile = "/mnt/ScratchPool/gisv61/ver2_gaps_play/datasets/drosophila/drosophila.contig"
	core = Preprocessor(clusterFile, contigFile, scaffoldFile)
